var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var steps = 31
var array = create2DArray(steps, steps, 0, false)

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth * 0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < steps; i++) {
    for (var j = 0; j < steps; j++) {
      fill(128 + 128 * abs(i - Math.floor(steps * 0.5)) * 0.035 * abs(j - Math.floor(steps * 0.5)) * array[i][j] * sin(frameCount * 0.1))
      push()
      translate(windowWidth * 0.5 + + (j - Math.floor(steps * 0.5)) * boardSize * (1 / steps) * 0.85, windowHeight * 0.5 + (i - Math.floor(steps * 0.5)) * boardSize * (1 / steps) * 0.85)
      rect(
        0,
        0,
        boardSize * 0.8 * (1 / steps) * sin(frameCount * 0.025 + abs(j - Math.floor(steps * 0.5)) * sin(frameCount * 0.01 + 0.01 * abs(i - Math.floor(steps * 0.5)) * sin(frameCount * 0.1)) * Math.PI),
        boardSize * 0.8 * (1 / steps) * sin(frameCount * 0.025 + abs(i - Math.floor(steps * 0.5)) * sin(frameCount * 0.01 + 0.01 * abs(j - Math.floor(steps * 0.5)) * sin(frameCount * 0.1)) * Math.PI)
      )
      pop()
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = Math.random()
      }
    }
    array[i] = columns
  }
  return array
}
